package com.example.myapplication.ui.newfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.example.myapplication.ui.dashboard.DashboardViewModel;

public class Lecture4Fragment extends Fragment {

    private Lecture4ViewModel lecture4ViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState){
        lecture4ViewModel =
                ViewModelProviders.of(this).get(Lecture4ViewModel.class);
        View root = inflater.inflate(R.layout.fragment_lecture4, container, false);
        final TextView textView = root.findViewById(R.id.text_lecture4);
        lecture4ViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}
