package com.example.myapplication.ui.newfragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.ui.home.HomeViewModel;

public class Lecture4ViewModel extends ViewModel {

    private MutableLiveData <String> mText;

    public Lecture4ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is lecture4 fragment");
    }

    public LiveData <String> getText() {
        return mText;
    }
}
